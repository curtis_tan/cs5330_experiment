#include<stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

struct timespec ts;

void printArray(int* arr, int low, int high) {
  for (int i = low; i <= high; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}

void makeArrayOfRandomInts(int* arr, int size, int max) {
  for (int i = 0; i < size; i++) {
    arr[i] = rand() % max;
  }
}

void makeArrayOfAlmostSortedInts(int* arr, int size, bool isAscending) {
  int target = size / 2;
  for (int i = 0; i < size; i++) {
    int value = i + 1;
    if (rand() % size == target) {
      value = -value;
    }
    if (!isAscending) {
      value = -value;
    }
    arr[i] = value;
  }
}

void makeArrayCopy(int* arr, int* newArr, int size) {
  for (int i = 0; i < size; i++) {
    newArr[i] = arr[i];
  }
}

bool isSorted(int* arr) {
  int size = sizeof(arr) / sizeof(int);
  if (size > 1) {
    int current = arr[0];
    for (int i = 1; i < size; i++) {
      if (arr[i] < current) {
        return false;
      }
      current = arr[i];
    }
  }
  return true;
}

long getNanos(void) {
  timespec_get(&ts, TIME_UTC);
  return (long) ts.tv_sec * 1000000000L + ts.tv_nsec;
}

void swap(int* x, int* y) {
  int temp = *x;
  *x = *y;
  *y = temp;
}

void insertionSort(int* arr, int low, int high) {
  for (int i = low + 1; i <= high; i++) {
    for (int j = i; j > low && arr[j] < arr[j - 1]; j--) {
      swap(&arr[j], &arr[j - 1]);
    }
  }
}

void singlePivotQuickSort(int* arr, int low, int high) {
  // Small sized arrays uses insertion sort
  if (high - low < 17) {
    insertionSort(arr, low, high);
    return;
  } 

  if (low < high) {
    int size = high - low;

    // median indexes
    int sixth = size / 6;
    int m1 = low + sixth;
    int m2 = m1 + sixth;
    int m3 = m2 + sixth;
    int m4 = m3 + sixth;
    int m5 = m4 + sixth;

    // 5-element sorting network
    if (arr[m1] > arr[m2]) { 
      swap(&arr[m1], &arr[m2]);
    }
    if (arr[m4] > arr[m5]) { 
      swap(&arr[m4], &arr[m5]); 
    }
    if (arr[m1] > arr[m3]) { 
      swap(&arr[m1], &arr[m3]);
    }
    if (arr[m2] > arr[m3]) { 
      swap(&arr[m2], &arr[m3]);
    }
    if (arr[m1] > arr[m4]) { 
      swap(&arr[m1], &arr[m4]);
    }
    if (arr[m3] > arr[m4]) { 
      swap(&arr[m3], &arr[m4]);
    }
    if (arr[m2] > arr[m5]) { 
      swap(&arr[m2], &arr[m5]);
    }
    if (arr[m2] > arr[m3]) { 
      swap(&arr[m2], &arr[m3]);
    }
    if (arr[m4] > arr[m5]) { 
      swap(&arr[m4], &arr[m5]); 
    }

    int pivot = arr[m3];
    swap(&arr[m3], &arr[high]);
    int i = low - 1;
    for (int j = low; j < high; j++) {
      if (arr[j] < pivot) {
        i++;
        swap(&arr[i], &arr[j]);
      }
    }
    swap(&arr[i + 1], &arr[high]);

    singlePivotQuickSort(arr, low, i);
    singlePivotQuickSort(arr, i + 2, high);
  }
}

void dualPivotQuickSort(int* arr, int low, int high) {
  // Small sized arrays uses insertion sort
  if (high - low < 17) {
    insertionSort(arr, low, high);
    return;
  }

  int size = high - low;

  // median indexes
  int sixth = size / 6;
  int m1 = low + sixth;
  int m2 = m1 + sixth;
  int m3 = m2 + sixth;
  int m4 = m3 + sixth;
  int m5 = m4 + sixth;

  // 5-element sorting network
  if (arr[m1] > arr[m2]) { 
    swap(&arr[m1], &arr[m2]);
  }
  if (arr[m4] > arr[m5]) { 
    swap(&arr[m4], &arr[m5]); 
  }
  if (arr[m1] > arr[m3]) { 
    swap(&arr[m1], &arr[m3]);
  }
  if (arr[m2] > arr[m3]) { 
    swap(&arr[m2], &arr[m3]);
  }
  if (arr[m1] > arr[m4]) { 
    swap(&arr[m1], &arr[m4]);
  }
  if (arr[m3] > arr[m4]) { 
    swap(&arr[m3], &arr[m4]);
  }
  if (arr[m2] > arr[m5]) { 
    swap(&arr[m2], &arr[m5]);
  }
  if (arr[m2] > arr[m3]) { 
    swap(&arr[m2], &arr[m3]);
  }
  if (arr[m4] > arr[m5]) { 
    swap(&arr[m4], &arr[m5]); 
  }

  int leftPivot = arr[m2];
  int rightPivot = arr[m4];

  bool isDifferentPivots = leftPivot != rightPivot;

  arr[m2] = arr[low];
  arr[m4] = arr[high];

  int less = low + 1;
  int great = high - 1;

  for (int k = less; k <= great; k++) {
    if (!isDifferentPivots && arr[k] == leftPivot) {
      continue;
    }
    if (arr[k] < leftPivot) {
      swap(&arr[k], &arr[less]);
      less++;
    } else if (!isDifferentPivots || arr[k] > rightPivot) {
      while (arr[great] > rightPivot && k < great) {
        great--;
      }
      swap(&arr[k], &arr[great]);
      great--;
      
      if (arr[k] < leftPivot) {
        swap(&arr[k], &arr[less]);
        less++;
      }
    }
  }

  // swap
  arr[low] = arr[less - 1];
  arr[less - 1] = leftPivot;
  arr[high] = arr[great + 1];
  arr[great + 1] = rightPivot;

  // left and right parts
  dualPivotQuickSort(arr, low, less - 2);
  dualPivotQuickSort(arr, great + 2, high);

  // equal elements
  if (great - less > size - 13 && isDifferentPivots) {
    for (int k = less; k <= great; k++) {
      if (arr[k] == leftPivot) {
        swap(&arr[k], &arr[less]);
        less++;
      } else if (arr[k] == rightPivot) {
        swap(&arr[k], &arr[great]);
        great--;

        if (arr[k] == leftPivot) {
          swap(&arr[k], &arr[less]);
          less++;
        }
      }
    }
  }

  // center part
  if (isDifferentPivots) {
    dualPivotQuickSort(arr, less, great);
  }
}

int main(void) {
  int numberOfBigRuns = 5;
  int numberOfRuns = 20;
  int size = 125;

  long start = 0;
  long end = 0;

  int* arrA = (int*) malloc(size * sizeof(int));
  int* arrB = (int*) malloc(size * sizeof(int));

  timespec_get(&ts, TIME_UTC);
  srand(ts.tv_nsec);

  for (int j = 0; j < numberOfBigRuns; j++) {
    long singlePivotSumTiming = 0;
    long dualPivotSumTiming = 0;

    for (int i = 0; i < numberOfRuns; i++) {
      //makeArrayOfRandomInts(arrA, size, size / duplicates);
      makeArrayOfAlmostSortedInts(arrA, size, false);
      makeArrayCopy(arrA, arrB, size);

      start = getNanos();
      singlePivotQuickSort(arrA, 0, size - 1);
      end = getNanos();

      if (!isSorted(arrA)) {
        printf("Single pivot quick sort failed to sort array!\n");
        free(arrA);
        free(arrB);
        return 1;
      } else {
        singlePivotSumTiming += end - start;
      }

      start = getNanos();
      dualPivotQuickSort(arrB, 0, size - 1);
      end = getNanos();

      if (!isSorted(arrB)) {
        printf("Dual pivot quick sort failed to sort array!\n");
        free(arrA);
        free(arrB);
        return 2;
      } else {
        dualPivotSumTiming += end - start;
      }
    }

    double singlePivotAverageTiming = (double) singlePivotSumTiming / numberOfRuns;
    double dualPivotAverageTiming = (double) dualPivotSumTiming / numberOfRuns;

    printf("\nAverage timings:\n");
    printf("Single pivot: %f\n", singlePivotAverageTiming);
    printf("Dual pivot: %f\n\n", dualPivotAverageTiming);
  }

  free(arrA);
  free(arrB);
  return 0;
}


